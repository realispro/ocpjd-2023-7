package lab.ocp;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class FormatMain {

    public static void main(String[] args) {
        System.out.println("Let's format!");

        // **** number format
        double value = 0.99;
        //123456789.1234567;

        Locale locale = new Locale("en", "UK");// = Locale.forLanguageTag("pl");
        //new Locale("en", "PL");
        //Locale.ITALY;
        //Locale.getDefault();
        System.out.println("locale = " + locale);

        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        //.getPercentInstance(locale);
        //.getInstance(locale);
        format.setMaximumFractionDigits(4);
        //format.setMinimumFractionDigits(20);
        //format.setMinimumIntegerDigits(20);
        String valueString = format.format(value);
        System.out.println("valueString = " + valueString);

        // **** date format
        // ---- legacy

        Date date = new Date(); // millis in EPOC - unix time: since 1/1/1970 00:00:00 GMT

        Calendar calendar = Calendar.getInstance();
        //calendar.setTime(date);
        calendar.set(2023, 6, 12, 13, 20, 59);
        calendar.add(Calendar.MONTH, 7);
        date = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("E yyyy*MM*dd w D hh*mm*ss*SS X G", locale);
        //DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, locale);
        String timestamp = dateFormat.format(date);
        System.out.println("[legacy] timestamp = " + timestamp);

        // ---- java.time
        LocalDate ld = LocalDate.of(2023, 7, 12).plusMonths(7);
        LocalTime lt = LocalTime.now().plus(1, ChronoUnit.NANOS);
        LocalDateTime ldt = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E yyyy*MM*dd w D hh*mm*ss*SS G", locale);
        //DateTimeFormatter.ISO_DATE_TIME;

        timestamp = formatter.format(ldt);
        System.out.println("[java.time] timestamp = " + timestamp);

        Period period = Period.between(
                LocalDate.now(),
                LocalDate.of(2023, 12, 24));
        System.out.println("period = " + period);

        // -- zones
        ZoneId warsawZone = ZoneId.of("Europe/Warsaw");
        ZoneId singaporeZone = ZoneId.of("Asia/Singapore");

        ZonedDateTime warsawStart = ZonedDateTime.now(warsawZone);
        System.out.println("warsawStart = " + warsawStart);
        ZonedDateTime warsawLanding = warsawStart.plusHours(14);
        System.out.println("warsawLanding = " + warsawLanding);
        ZonedDateTime singaporeLanding = warsawLanding.withZoneSameInstant(singaporeZone);
        System.out.println("singaporeLanding = " + singaporeLanding);

        // **** L10N - localization  I18N - internationalization
        // pl_PL
        // ocpjd_pl_PL.properties
        // ocpjd_pl.properties
        // ocpjd.properties
        ResourceBundle bundle = ResourceBundle.getBundle("ocpjd", locale);
        String heyUniverse = bundle.getString("hey.universe");
        System.out.println("heyUniverse = " + heyUniverse);

        Period p = Period.between(LocalDate.now(), LocalDate.of(2023,7, 19));
                //Period.ofDays(7);

        p = p.withYears(2);
        System.out.println("p = " + p);



        System.out.println("done.");
    }
}
