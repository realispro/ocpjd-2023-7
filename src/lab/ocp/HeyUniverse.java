// one line comment
/*
multiline
comment
 */
package lab.ocp;

import static lab.ocp.calc.Calculator.multiply;

import java.util.Arrays;

public class HeyUniverse {

    final static public void main(String... xyz){
        System.out.println("Hey Universe!");

        long age = 2_147_483_647L;
        System.out.println("age=" + age);
        
        float price = 123.456F;
        System.out.println("price = " + price);

        char c = 0b111110001; //'x';
        System.out.println("c = " + c);

        boolean weekend = false;
        System.out.println("weekend = " + weekend);

        Integer i = new Integer(102); // autoboxing: Integer.valueOf(102);
        Integer i2 = 102;
        int ii = i; //unboxing: i.intValue();
        System.out.println("i = " + i);
        System.out.println("i==i2:" + (i==i2));

        int[] values = /*new int[]*/  {1,2,3,4,5};
        /*values[0] = 1;
        values[1] = 2;
        values[2] = 3;
        values[3] = 4;
        values[4] = 5;*/

        values[values.length-1] = 102;

        System.out.println("values = " + Arrays.toString(values));

        final byte v1 = 6;
        byte v2 = 7;

        var v3 = (byte) (v1 * v2);

        //v3 = (v1<v2) ? v1 : v2; // ternary operator

        switch(v3){ //int, short, byte, char, String, enum
            case v1:
                System.out.println("v3==v1");
                break;
            case 35:
                System.out.println("35");
                break;
            case 50:
                System.out.println("50");
                break;
            default:
                System.out.println("default");
                break;
        }

        String season = "xyz";

        String translation = switch (season){
            case "spring" -> "wiosna";
            case "summer" -> "lato";
            case "autumn" -> "jesien";
            default -> "zima";
        };

        System.out.println("translation = " + translation);


        int size = 15;
        int[][] result = new int[size][size];

        OUTER:
        for(int x=1; x<=size; x++){
            for(int y=1; y<=size; y++){
                if(y==13){
                    break OUTER;
                }
                result[x-1][y-1] = multiply(x, y, 30);
                        // Calculator.multiply(x, y);
                        // lab.ocp.calc.Calculator.multiply(x, y);
                        // HeyUniverse.multiply(x, y);
                        // multiply(x,y);
            }
        }

        for( var row : result){
            for ( var v : row){
                System.out.print(v + "\t");
            }
            System.out.println();
        }
    }

    /*public static int multiply(float iks, float igrek){
        return (int)(iks * igrek);
    }*/



}

