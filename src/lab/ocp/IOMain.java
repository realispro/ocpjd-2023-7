package lab.ocp;

import lab.ocp.calc.BetterCalculator;
import lab.ocp.generics.Box;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class IOMain {

    public static void main(String[] args) {
        System.out.println("let's read and write!");


        try (BufferedWriter bw = new BufferedWriter(new FileWriter(createNIOFile(), true))) { // try-with-reources

            // 2. data streams
            String timestamp = LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME);
            bw.write(timestamp + "\n");

        } catch (IOException e) {
            e.printStackTrace();
        } /*finally {
            try {
                if(bw!=null) {
                    bw.close();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }*/

        try (BufferedReader br = new BufferedReader(new FileReader(createNIOFile()))) {

            String line;

            while ((line = br.readLine()) != null) {
                System.out.println("line = " + line);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // 3. serialization
        Box<Object> box = new Box<>();
        box.setObject(new BetterCalculator());

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("box.serialized")))){
            oos.writeObject(box);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("box.serialized")))){
            Object boxObject = ois.readObject();
            System.out.println("boxObject = " + boxObject);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }


        System.out.println("done.");

    }

    static File createNIOFile() throws IOException {

        Path dir = Paths.get("mydir", "innerdir");
        if (Files.exists(dir)) {
            System.out.println("directory exists");
        } else {
            System.out.println("creating directory");
            Files.createDirectories(dir);
        }

        Path file = Paths.get(dir.toString(), "myfile.txt");
        if (Files.exists(file)) {
            System.out.println("file exists");
        } else {
            System.out.println("creating file");
            Files.createFile(file);
        }

        return file.toFile();

    }


    static File createFile() throws IOException {
        File dir = new File("mydir" + File.separator + "innerdir");

        if (dir.exists()) {
            System.out.println("directory exists.");
        } else {
            System.out.println("creating directory");
            dir.mkdirs();
        }

        File file = new File(dir, "myfile.txt");
        if (file.exists()) {
            System.out.println("file exists");
        } else {
            System.out.println("creating file");
            file.createNewFile();
        }

        return dir;
    }
}
