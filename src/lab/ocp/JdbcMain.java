package lab.ocp;

import lab.ocp.zoo.Animal;
import lab.ocp.zoo.Kiwi;
import lab.ocp.zoo.Oyster;
import lab.ocp.zoo.sea.Shark;
import lab.ocp.zoo.sea.Tuna;

import java.sql.*;

public class JdbcMain {

    public static void main(String[] args) {

        Animal animal = new Tuna("Tunczyk", 500);
        Animal animal2 = new Shark("Rekin", 500);
        Animal animal3 = new Oyster("Malz", 500);


        try(Connection connection = getConnection()){

            connection.setAutoCommit(false);

            System.out.println("db version: " + connection.getMetaData().getDatabaseProductVersion());

            PreparedStatement statement = connection.prepareStatement(
                    "insert into animal (name, size, type) values (?,?,?)");

            // animal1
            statement.setString(1, animal.getName());
            statement.setInt(2, animal.getSize());
            statement.setString(3, animal.getClass().getSimpleName());

            statement.addBatch();
            //int rowsCount = statement.executeUpdate();
            //System.out.println("rows updated: " + rowsCount);

            // animal2
            statement.setString(1, animal2.getName());
            statement.setInt(2, animal2.getSize());
            statement.setString(3, animal2.getClass().getSimpleName());

            statement.addBatch();
            //rowsCount = statement.executeUpdate();
            //System.out.println("rows updated: " + rowsCount);

            // animal3
            statement.setString(1, animal3.getName());
            statement.setInt(2, animal3.getSize());
            statement.setString(3, animal3.getClass().getSimpleName());

            statement.addBatch();
            //rowsCount = statement.executeUpdate();
            //System.out.println("rows updated: " + rowsCount);

            statement.executeBatch();

            connection.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try(Connection connection = getConnection()){

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select name, size, type from animal");
            while(rs.next()){
                String name = rs.getString(1);
                int size = rs.getInt("size");
                String type = rs.getString("type");
                System.out.println(name + " " + size + " " + type);
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public static Connection getConnection(){

        String url = "jdbc:mysql://localhost:3306/lab";
        String user = "root";
        String password = "mysql";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
