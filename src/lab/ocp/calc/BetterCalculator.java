package lab.ocp.calc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class BetterCalculator extends Calculator{

    static {
        System.out.println("BetterCalculator static block");
    }

    static Collection<String> collection = new ArrayList<>();

    static {
        collection.add("abc");
        collection.add("xyz");
    }

    {
        System.out.println("BetterCalculator initialization block");
    }

    public BetterCalculator(){
        super(0.0);
        System.out.println("BetterCalculator constructor");
    }

    @Override
    protected void save() {
        System.out.println("saving BetterCalculator state");
    }

    public void power(int operand){
        double value = this.result;

        if(operand==0){
            this.result = 1;
        } else {
            for (int i = 1; i < operand; i++) {
                this.result = this.result * value;
            }
        }
    }

    @Override
    void divide(double operand) /*throws CalculatorException*/ {
        if(operand==0){
            throw new CalculatorException("pamietaj chol... nie dziel przez zero");
        }
        super.divide(operand);
    }

    @Override
    public boolean charge() {
        System.out.println("BetterCalculator charging");
        return new Random(System.currentTimeMillis()).nextBoolean();
    }

    @Override
    public void doNothing() {
    }
}
