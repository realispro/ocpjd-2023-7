package lab.ocp.calc;

public abstract class Calculator /*extends Object */
        implements Device{

    static {
        System.out.println("Calculator static block");
    }

    {
        System.out.println("Calculator initialization block");
    }

    protected double result; // = 0.0


    public Calculator(double result){
        super();
        // initialization block
        System.out.println("Calculator args constructor");
        this.result = result;
    }

    public Calculator(){
        this(0.0);
        System.out.println("Calculator noargs constructor");
    }


    protected abstract void save();


    void add(double operand){
        result += operand;
    }

    void subtract(double operand){
        result -= operand;
    }

    void multiply(double operand){
        result *= operand;
    }

    void divide(double operand) throws CalculatorException {
        result /= operand;
    }

    public double getResult() {
        return result;
    }

    /*public void setResult(double result){
        this.result = result;
    }*/

    public static int multiply(float first, float... operands){
        float r = first;
        for(float operand : operands){
            r = r * operand;
        }

        return (int)r;
    }
}
