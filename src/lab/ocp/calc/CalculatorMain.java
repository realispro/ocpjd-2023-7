package lab.ocp.calc;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

public class CalculatorMain {

    public static void main(String[] args) {
        System.out.println("Let's calculate!");

        Calculator c1 = new BetterCalculator();
        //Calculator c2 = new Calculator(3.3);



        try(Connection connection = null;){ // try-with-resources: AutoCloseable
            c1.divide(1);
            c1.hashCode();
            c1.add(5.5);
            c1.subtract(1.3333);
            c1.multiply(4.5);
            //System.exit(102);
        } catch (NullPointerException | CalculatorException e){
            e.printStackTrace();
            //e = new CalculatorException("just a joke");
            throw new RuntimeException(e);
        } catch (RuntimeException e){
            e.printStackTrace();
            e = new IllegalArgumentException("just a joke :)");
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            System.out.println("in finally block");
        }



        if(c1 instanceof BetterCalculator) {
            ((BetterCalculator) c1).power(8);
        }

        c1.save();
        c1.charge();

        System.out.println("c1.result=" + c1.getResult());



        Device device = () -> LocalDate.now().isLeapYear();

                /*new Device() {
            @Override
            public boolean charge() {
                return false;
            }
        };*/
        System.out.println("charged? " + device.charge());

        Device.getDescription();

        System.out.println("done.");
    }

}
