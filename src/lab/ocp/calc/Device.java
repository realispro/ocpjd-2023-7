package lab.ocp.calc;

@FunctionalInterface
public interface Device {

    /*public abstract*/ boolean charge();

    default void doNothing(){
        System.out.println("doing literally nothing " + getSomething());
    }

    private int getSomething(){
        return 1;
    }

    static String getDescription(){
        staticPrivateMethod();
        return "Device description";
    }

    private static void staticPrivateMethod(){}

}
