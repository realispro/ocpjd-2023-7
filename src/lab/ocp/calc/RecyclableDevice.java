package lab.ocp.calc;

import java.io.Serializable;

@FunctionalInterface
public interface RecyclableDevice extends Device, Serializable {

    default void recycle(){}
}
