package lab.ocp.concurrency.alarm;

public abstract class Alarm implements Runnable{

    public void alarm(){

        specificAlarm();
    }

    protected abstract void specificAlarm();

    @Override
    public void run() {
        alarm();
    }
}
