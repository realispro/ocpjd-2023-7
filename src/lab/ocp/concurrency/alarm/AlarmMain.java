package lab.ocp.concurrency.alarm;


import lab.ocp.concurrency.ThreadNamePrefixPrintStream;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AlarmMain {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        ExecutorService es = Executors.newFixedThreadPool(1);

        List<Alarm> alarms = new ArrayList<>();
        alarms.add(new Beeper());
        alarms.add(new Flash());

        alarms.forEach(a->es.execute(a));

        es.shutdown();

        System.out.println("done.");
}
}
