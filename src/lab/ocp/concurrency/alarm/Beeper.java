package lab.ocp.concurrency.alarm;

public class Beeper extends Alarm {

    protected void specificAlarm() {
        for(int i=0; i<20; i++){
            System.out.println("BEEP");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
