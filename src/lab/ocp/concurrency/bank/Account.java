package lab.ocp.concurrency.bank;

import java.util.concurrent.atomic.AtomicInteger;

public class Account {

    private AtomicInteger amount;

    public Account(int amount) {
        this.amount = new AtomicInteger(amount);
    }

    public void deposit(int value){
        // 1. get current value
        // 2. calculate new value
        // 3. assign calculated value
        amount.addAndGet(value);

    }

    public void withdraw(int value){
        amount.addAndGet(-value);
    }

    public int getAmount() {
        return amount.get();
    }
}
