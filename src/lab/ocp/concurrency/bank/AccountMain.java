package lab.ocp.concurrency.bank;


import lab.ocp.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AccountMain {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        Account a = new Account(1000);

        DepositTask depositTask = new DepositTask(a);
        WithdrawTask withdrawTask = new WithdrawTask(a);

        ExecutorService es = Executors.newFixedThreadPool(2);

        es.execute( () -> depositTask.deposit());
        es.execute( () -> withdrawTask.withdraw());

        es.shutdown();

        System.out.println("account: " + a.getAmount());

    }
}
