package lab.ocp.generics;

import java.io.Serial;
import java.io.Serializable;

public class Box<T> implements Serializable {

    private static final long serialVersionUID = -8666885909037666465L;

    private transient T object;

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "Box{" +
                "object=" + object +
                '}';
    }

    public static <T> Box<T> createEmptyBox(){
        return new Box<T>();
    }

    public static <X> Box<X> createBoxWithContent(X content){
        Box<X> box = new Box<>();
        box.setObject(content);
        return box;
    }
}
