package lab.ocp.generics;

public class BoxFactory {

    public static <X> Box<X> createEmptyBox(){
        return new Box<X>();
    }

    public static <X> Box<X> createBoxWithContent(X content){
        Box<X> box = new Box<>();
        box.setObject(content);
        return box;
    }

}
