package lab.ocp.generics;

import lab.ocp.zoo.sea.Shark;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GenericsMain {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        System.out.println("Let's generalize!");

        Box<Integer> box = Box.createBoxWithContent(102);
                //.createEmptyBox();
                //new IntegerBox();
                //new NumberBox<Number>();
                //new Box<Integer>();
        box.setObject(102);

        // type erasure
        //Method method = box.getClass().getDeclaredMethod("setObject", Object.class);
        //method.invoke(box, 303);

        Number value = box.getObject();
        System.out.println("value = " + value);
        
        
        Box<String> boxString = new Box<String>();
        boxString.setObject("Hey Universe!");

        System.out.println("boxString.getObject() = " + boxString.getObject());

        Box<Object> objectBox = new Box<Object>();
        Box<Number> numberBox = new Box<Number>();
        Box<Integer> intBox = new Box<Integer>();
        Box<Float> floatBox = new Box<Float>();
        addObject(objectBox);
        printObject(floatBox);

        System.out.println("done.");
    }


    // PECS -> producer extends, consumer super
    public static void addObject(Box<? super Integer> box){
        box.setObject(12);
        Object content = box.getObject();
    }

    public static void printObject(Box<? extends Number> box){
        //box.setObject(12);
        Number content = box.getObject();
        System.out.println("content = " + content);

    }
}
