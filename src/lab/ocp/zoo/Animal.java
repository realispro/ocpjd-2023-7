package lab.ocp.zoo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public abstract class Animal implements Comparable<Animal>{

    static List<String> allNames = List.of("Johny", "Jimmi", "Joe", "Janek", "Jozek", "Jurek");

    protected String name;
    protected int size;

    protected List<String> kids;

    {
        kids = new ArrayList<>();
        int count = new Random().nextInt(allNames.size());
        for(int i=0; i<count; i++){
           kids.add(allNames.get(new Random().nextInt(allNames.size())));
        }
    }

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public void eat(String food){
        System.out.println(name + " is eating " + food);
    }

    public abstract void move();

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public List<String> getKids() {
        return kids;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Animal animal = (Animal) o;

        if (getSize() != animal.getSize()) return false;
        return getName() != null ? getName().equals(animal.getName()) : animal.getName() == null;
    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + getSize();
        return result;
    }

    @Override
    public int compareTo(Animal a) {
        System.out.println("comparing " + this + " and " + a);
        return this.size-a.size;
    }
}
