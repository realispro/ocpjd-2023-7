package lab.ocp.zoo;

import java.util.Comparator;

public class AnimalByNameComparator implements Comparator<Animal> {
    @Override
    public int compare(Animal a1, Animal a2) {
        System.out.println("comparing " + a1 + " and " + a2);
        int diff = a1.getName().compareTo(a2.getName());
        if(diff==0){
            return a2.getSize()-a1.getSize();
        } else {
            return diff;
        }
    }
}
