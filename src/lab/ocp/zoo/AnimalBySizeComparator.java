package lab.ocp.zoo;

import java.util.Comparator;

public class AnimalBySizeComparator implements Comparator<Animal> {
    @Override
    public int compare(Animal a1, Animal a2) {
        return a1.getSize() - a2.getSize();
    }
}
