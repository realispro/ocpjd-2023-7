package lab.ocp.zoo;

public class Oyster extends Fish{

    private static String resource = "mass";

    public Oyster(String name, int size) {
        super(name, size);
    }


    public static class Pearl{

        private String content;

        public Pearl(){
            this.content = "sand";
        }

        public void grow(){
            this.content = resource + "[" + this.content + "]";
        }

        public String getContent() {
            return content;
        }
    }
}
