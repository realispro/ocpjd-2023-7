package lab.ocp.zoo;

import lab.ocp.zoo.sea.Shark;
import lab.ocp.zoo.sea.Tuna;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ZooMain {

    public static void main(String[] args) {

        System.out.println("Let's visit zoo!");

        Animal george = new Shark("George", 500);
        Animal bielik = new Eagle("Bielik", 50);
        Animal zenek = new Kiwi("Zenek", 5);
        Animal jimmi = new Tuna("Biden", 250);
        Animal joe = new Eagle("Biden", 45);

        Animal[] animals = new Animal[]{ george, bielik, zenek, jimmi, joe };

        Animal nemo = new Shark("Nemo", 300);

        Animal[] animals2 = new Animal[animals.length+1];
        System.arraycopy(animals, 0, animals2, 0, animals.length);
        animals2[animals2.length-1] = nemo;

        List<Animal> animalsCollection = new ArrayList<>(50);
        animalsCollection.add(george);
        animalsCollection.set(0, bielik); // List
        animalsCollection.add(zenek);
        animalsCollection.add(jimmi);
        animalsCollection.add(joe);
        animalsCollection.add(george);
        animalsCollection.add(nemo);

        // ------ sorting
        Comparator<Animal> comparator = (a1, a2) -> a1.getSize()-a2.getSize();
        animalsCollection.sort(comparator);

        System.out.println("animals collection size: " + animalsCollection.size());

        //animalsCollection.remove(3);
        System.out.println("animalsCollection.get(3) = " + animalsCollection.get(3));

        // -------- filtering
        Predicate<Animal> predicate = animal -> animal.getName().equals("Nemo");
        animalsCollection.removeIf(predicate);

        // -------- reviewing
        Consumer<Animal> consumer = animal -> System.out.println("[lambda consumer] " + animal);
        animalsCollection.forEach(consumer);

        Animal george2 = new Tuna("George", 500);

        Set<Animal> animalsSet = new TreeSet<>(comparator);
                //new HashSet<>();
        animalsSet.add(george);
        animalsSet.add(bielik);
        animalsSet.add(zenek);
        animalsSet.add(jimmi);
        animalsSet.add(joe);
        animalsSet.add(george2);
        animalsSet.add(nemo);

        System.out.println("animalsSet.size() = " + animalsSet.size());

        for(Animal animal : animalsSet){
            System.out.println("[set] animal = " + animal);
        }

        
        Oyster oyster = new Oyster("nemo", 1);
        Oyster.Pearl pearl = new Oyster.Pearl();
        pearl.grow();
        pearl.grow();
        pearl.grow();

        System.out.println("pearl.getContent() = " + pearl.getContent());


        class LocalClass{

        }

        LocalClass localClass = new LocalClass();


        System.out.println("done.");
    }


    public static void doNothing(){
        class LocalClass{

        }

        LocalClass localClass = new LocalClass();
    }
}
