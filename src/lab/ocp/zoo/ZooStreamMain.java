package lab.ocp.zoo;

import lab.ocp.zoo.sea.Shark;
import lab.ocp.zoo.sea.Tuna;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ZooStreamMain {

    public static void main(String[] args) {
        System.out.println("Let's stream animals!");

        Animal george = new Shark("George", 500);
        Animal bielik = new Eagle("Bielik", 50);
        Animal zenek = new Kiwi("Zenek", 5);
        Animal jimmi = new Tuna("Biden", 250);
        Animal joe = new Eagle("Biden", 45);
        Animal nemo = new Shark("Nemo", 300);


        List<Animal> animalsCollection = new ArrayList<>(50);
        animalsCollection.add(george);
        animalsCollection.add(bielik); // List
        animalsCollection.add(zenek);
        animalsCollection.add(jimmi);
        animalsCollection.add(joe);
        animalsCollection.add(george);
        animalsCollection.add(nemo);

        Stream<Animal> stream = animalsCollection
                //.stream();
                .parallelStream();

        if(!stream.isParallel()){
            stream = stream.parallel();
        }


        LocalTime start = LocalTime.now();
        var result =
        stream
                // ********** intermediate ops
                        .sorted( (a1,a2) -> a1.getName().compareTo(a2.getName()))
                        .filter(animal -> animal.getSize()>200)
                        .map(animal -> animal.getName())
                        .distinct()
                        //.limit(0)
                // ********** terminal op
                        //.count();
                        //.collect(Collectors.toList());
                        .anyMatch(name -> name.contains("o"));
                        //.findFirst();

        LocalTime end = LocalTime.now();
        Duration duration = Duration.between(start, end);
        System.out.println("duration = " + duration);

        Supplier<RuntimeException> supplier = () -> new IllegalArgumentException("animal expected");
        System.out.println("result = " + result);

        var mapResult =
        animalsCollection.stream()
                        //.map(animal -> animal.getKids())
                        .flatMap(animal -> animal.getKids().stream())
                        .collect(Collectors.toList()); // .toList; JDK >= 16


        Stream<Integer> integerStream = Stream.iterate(1, i -> i+1);
                //.generate(()->102);

        var integerStreamResult =
        integerStream
                .limit(10)
                .reduce(0, (sum, i) -> sum+i);
                //.forEach(i->System.out.println(i));

        System.out.println("integerStreamResult = " + integerStreamResult);

        IntStream intStream = IntStream.iterate(1, i -> i+1);
        var intStreamResult = intStream
                .limit(10)
                //.sum();
                .average();
        System.out.println("intStreamResult = " + intStreamResult);

        // void accept(T t); void println(Object o)
        Consumer<Animal> consumer = System.out::println;
                //animal -> System.out.println(animal);
        animalsCollection
                .stream()
                        .forEach(consumer);


        System.out.println("done.");
    }
}
