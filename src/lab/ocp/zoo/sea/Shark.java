package lab.ocp.zoo.sea;


import lab.ocp.zoo.Fish;

public class Shark extends Fish {
    public Shark(String name, int size) {
        super(name, size);
    }
}
