package lab.ocp.zoo.sea;

import lab.ocp.zoo.Fish;

public class Tuna extends Fish {

    public Tuna(String name, int size) {
        super(name, size);
    }
}
